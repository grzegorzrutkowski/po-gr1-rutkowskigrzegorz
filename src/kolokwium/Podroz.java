package kolokwium;

import java.time.LocalDate;
import java.util.ArrayList;

public class Podroz implements IData, IZarzadzaj {
    private LocalDate dataPodrozy;
    private ArrayList<SrodekLokomocji> planPodrozy;
    private double koszt = 0;

    public Podroz(){
        planPodrozy = new ArrayList<>();
        this.koszt = 0;
    }

    public LocalDate getDataPodrozy(){
        return dataPodrozy;
    }

    @Override
    public void UstawDate(LocalDate data) {
        dataPodrozy = data;
    }

    @Override
    public boolean SprawdzDate() {
        LocalDate tempdata = LocalDate.now();
        return tempdata.compareTo(this.dataPodrozy) > 0;
    }

    @Override
    public void DodajAutobus(int iloscMiejsc) {
        Autobus tempautobus = new Autobus(iloscMiejsc);
        planPodrozy.add(tempautobus);
        this.koszt += tempautobus.cenaBiletu;
    }

    @Override
    public void DodajPociag(int iloscMiejsc, int dlugoscTrasy) {
        Pociag temppociag = new Pociag(iloscMiejsc, dlugoscTrasy);
        planPodrozy.add(temppociag);
        this.koszt += temppociag.cenaBiletu;
    }

    @Override
    public void UsunOstatni() {
        if(planPodrozy.size() > 0)
            planPodrozy.remove(planPodrozy.size() - 1);
    }

    @Override
    public void Wyczysc() {
        planPodrozy.clear();
    }

    @Override
    public String toString() {
        StringBuilder test = new StringBuilder("");
        for(SrodekLokomocji temp: planPodrozy){
            test.append(temp.toString()).append("\n");
        }
        return test.toString();
    }
}
