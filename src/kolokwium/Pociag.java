package kolokwium;

public class Pociag extends SrodekLokomocji {
    private int dlugoscTrasy;

    public Pociag(int iloscMiejsc, int dlugoscTrasy){
        this.iloscMiejsc = iloscMiejsc;
        this.dlugoscTrasy = dlugoscTrasy;
        obliczCene();
    }

    public void obliczCene(){
        if(dlugoscTrasy > 150){
            this.cenaBiletu = this.dlugoscTrasy * 1.42;
        } else {
            super.obliczCene();
        }
    }

    @Override
    public String toString() {
        return this.getClass().getName() + " Ilość miejsc: " + this.iloscMiejsc + ", Cena biletu: " + this.cenaBiletu + ", Dlugosc trasy: " + this.dlugoscTrasy;
    }
}
