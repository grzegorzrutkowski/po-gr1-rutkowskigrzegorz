package kolokwium;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Aplikacja {
    public static void PokazMenu(){
        System.out.println("Zaplanuj swoją podróż!");
        System.out.println("[A] - dodaj autobus");
        System.out.println("[P] - dodaj pociąg");
        System.out.println("[U] - usuń ostatnią pozycję z planu podróży");
        System.out.println("[Z] - pokaż plan podróży");
        System.out.println("[D] - sprawdź datę podróży");
        System.out.println("[L] - ustaw datę podróży");
        System.out.println("[Q] - Zakończ działanie programu");
    }
    public static void main(String[] args) {
        String mychar = "";
        Scanner scanner = new Scanner(System.in);

        Podroz mojapodroz = new Podroz();
        while(!mychar.toUpperCase().equals("Q")){
            PokazMenu();
            System.out.println("Podaj znak");
            mychar = scanner.nextLine();
            switch (mychar.toUpperCase()) {
                case "A" -> {
                    System.out.println("Podaj ilość miejsc!");
                    int ilosc = scanner.nextInt();
                    if (ilosc > 0) {
                        mojapodroz.DodajAutobus(ilosc);
                        System.out.println("Autobus dodany");
                    } else
                        System.out.println("Ilość osób musi być wieksza od 0");
                }
                case "P" -> {
                    System.out.println("Podaj ilość miejsc!");
                    int iloscMiejsc = scanner.nextInt();
                    if (iloscMiejsc <= 0) {
                        System.out.println("Ilość osób musi być wieksza od 0");
                        break;
                    }
                    System.out.println("Podaj dlugosc trasy!");
                    int dlugoscTrasy = scanner.nextInt();
                    if (dlugoscTrasy <= 0) {
                        System.out.println("Dlugosc trasy musi być wieksza od 0");
                        break;
                    }
                    mojapodroz.DodajPociag(iloscMiejsc, dlugoscTrasy);
                }
                case "U" -> {
                    mojapodroz.UsunOstatni();
                    System.out.println("Ostatni element usuniety!");
                }
                case "Z" -> System.out.println(mojapodroz.toString());
                case "L" -> {
                    System.out.println("Podaj rok");
                    int rok = scanner.nextInt();
                    if (rok <= 0) {
                        System.out.println("Rok musi byc dodatni!");
                    }
                    System.out.println("Podaj miesiac");
                    int miesiac = scanner.nextInt();
                    if (miesiac <= 0) {
                        System.out.println("Miesiac musi byc dodatni!");
                    }
                    System.out.println("Podaj dzien");
                    int dzien = scanner.nextInt();
                    if (dzien <= 0) {
                        System.out.println("Dzień musi byc dodatni!");
                    }
                    mojapodroz.UstawDate(LocalDate.of(rok, miesiac, dzien));
                    System.out.println("Data ustawiona!");
                }
                case "D" -> System.out.println(mojapodroz.getDataPodrozy().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                default -> System.out.println("Brak takiej opcji");
            }
        }




    }
}
