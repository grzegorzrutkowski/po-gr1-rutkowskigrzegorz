package kolokwium;

import java.time.LocalDate;

public interface IData {
    void UstawDate(LocalDate data);
    boolean SprawdzDate();
}
