package kolokwium;

public class Autobus extends SrodekLokomocji {
    public Autobus(int iloscMiejsc){
        this.iloscMiejsc = iloscMiejsc;
        super.obliczCene();
    }

    @Override
    public String toString() {
        return this.getClass().getName() + " Ilość miejsc: " + this.iloscMiejsc + ", Cena biletu: " + this.cenaBiletu;
    }
}
