package pl.edu.uwm.wmii.rutkowski.laboratorium12;

import java.util.LinkedList;

public class zad1 {
    public static void main (String[] args) {
        LinkedList<String> ll
                = new LinkedList<String>();

        // Adding elements to the linked list
        ll.add("A");
        ll.add("B");
        ll.addLast("C");
        ll.addLast("D");

        System.out.println(ll);

        redukuj(ll, 2);
        System.out.println(ll);
    }

    public static void redukuj(LinkedList<String> pracownicy, int n){
        int ile = 0;
        for (int i = 0; i < pracownicy.size(); i++) {
            if(ile == n){
                ile = 0;
                pracownicy.remove(i);
                continue;
            }
            ile++;
        }

    }
}
