package pl.edu.uwm.wmii.rutkowski.laboratorium10;

import pl.imiajd.rutkowski.Student3;

import java.util.ArrayList;
import java.util.Collections;

public class TestStudent {
    public static void main(String[] args)
    {
        ArrayList<Student3> grupa = new ArrayList<>();

        grupa.add(new Student3("Rutkowski", 2000, 4, 18, 4.3));
        grupa.add(new Student3("Kowalski", 1990, 2, 15, 4.5));
        grupa.add(new Student3("Kowalski", 1992, 2, 15, 4.6));
        grupa.add(new Student3("Nowak", 1998, 10, 11, 3.0));
        grupa.add(new Student3("Jarosz", 1998, 10, 11, 3.0));

        for(Student3 el : grupa){
            System.out.println(el.toString());
        }

        Collections.sort(grupa);

        System.out.println("Po posortowaniu: ");

        for(Student3 el : grupa){
            System.out.println(el.toString());
        }
    }
}
