package pl.edu.uwm.wmii.rutkowski.laboratorium10;

import pl.imiajd.rutkowski.Osoba3;

import java.util.ArrayList;
import java.util.Collections;

public class TestOsoba {
    public static void main(String[] args)
    {
        ArrayList<Osoba3> grupa = new ArrayList<>();

        grupa.add(new Osoba3("Rutkowski", 2000, 4, 18));
        grupa.add(new Osoba3("Kowalski", 1990, 2, 15));
        grupa.add(new Osoba3("Kowalski", 1992, 2, 15));
        grupa.add(new Osoba3("Nowak", 1998, 10, 11));
        grupa.add(new Osoba3("Jarosz", 1998, 10, 11));

        for(Osoba3 el : grupa){
            System.out.println(el.toString());
        }

        Collections.sort(grupa);

        System.out.println("Po posortowaniu: ");

        for(Osoba3 el : grupa){
            System.out.println(el.toString());
        }
    }
}
