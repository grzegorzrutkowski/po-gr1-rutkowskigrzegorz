package pl.edu.uwm.wmii.rutkowski.laboratorium10;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class zad3 {
    public static void main(String[] args) throws FileNotFoundException
    {
        try {
            System.out.print("Podaj nazwe pliku : ");
            ArrayList<String> linie = new ArrayList<>();

            Scanner input = new Scanner(System.in);

            File file = new File(input.nextLine());

            input = new Scanner(file);


            while (input.hasNextLine()) {
                String line = input.nextLine();
                linie.add(line);
            }
            input.close();

            for(String linia : linie){
                System.out.println(linia);
            }

            Collections.sort(linie);

            for(String linia : linie){
                System.out.println(linia);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
