package pl.edu.uwm.wmii.rutkowski.laboratorium08;

import pl.imiajd.rutkowski.Osoba2;
import pl.imiajd.rutkowski.Pracownik2;
import pl.imiajd.rutkowski.Student2;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class zad2
{
    public static void main(String[] args)
    {
        Osoba2[] ludzie = new Osoba2[2];
        String[] imiona = {"Tomek", "Grzesiek"};
        LocalDate now = LocalDate.now();

        ludzie[0] = new Pracownik2("Kowalski ", imiona, now, false, 3000, now);
        ludzie[1] = new Student2("Rutkowski ", imiona, now, false, "Informatyka", 4.3);

        for (Osoba2 p : ludzie) {
            System.out.println(p.getNazwisko()  + p.getFormattedImiona() + " " + p.getFormattedPlec() + " urodzony: " + p.getFormattedData(p.getDataUrodzenia()) +  ": " + p.getOpis());
        }
    }
}


