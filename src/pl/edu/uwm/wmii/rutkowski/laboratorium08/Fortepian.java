package pl.edu.uwm.wmii.rutkowski.laboratorium08;

public class Fortepian extends Instrument{

    public Fortepian(String producent, String rokProdukcji) {
        super(producent, rokProdukcji);
    }

    @Override
    public String dzwiek() {
        return "trtrtr";
    }
}
