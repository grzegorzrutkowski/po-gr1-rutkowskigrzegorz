package pl.edu.uwm.wmii.rutkowski.laboratorium08;

public class Skrzypce extends Instrument{

    public Skrzypce(String producent, String rokProdukcji) {
        super(producent, rokProdukcji);
    }

    @Override
    public String dzwiek() {
        return "zzzzzz";
    }
}
