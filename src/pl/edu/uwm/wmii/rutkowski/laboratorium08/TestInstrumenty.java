package pl.edu.uwm.wmii.rutkowski.laboratorium08;

import java.util.ArrayList;

public class TestInstrumenty {
    public static void main(String[] args)
    {
        ArrayList<Instrument> instrumenty = new ArrayList<>();
        instrumenty.add(new Flet("lux flet", "2000"));
        instrumenty.add(new Fortepian("lux fortepian", "2015"));
        instrumenty.add(new Skrzypce("lux skrzypce", "2020"));
        instrumenty.add(new Fortepian("lux fortepian", "2033"));
        instrumenty.add(new Skrzypce("lux skrzypce", "2020"));

        for(Instrument e : instrumenty){
            System.out.println(e.toString() + " dzwiek: " + e.dzwiek());
        }
    }
}
