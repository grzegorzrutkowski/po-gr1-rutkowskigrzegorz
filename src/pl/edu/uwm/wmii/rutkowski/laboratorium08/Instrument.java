package pl.edu.uwm.wmii.rutkowski.laboratorium08;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

abstract class Instrument {
    private String producent;
    private LocalDate rokProdukcji;

    public Instrument(String producent, String rokProdukcji){
        this.producent = producent;

        DateTimeFormatter format = new DateTimeFormatterBuilder()
                .appendPattern("yyyy")
                .parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
                .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
                .toFormatter();
        this.rokProdukcji = LocalDate.parse(rokProdukcji, format);
    }

    public String getProducent() {
        return producent;
    }

    public LocalDate getRokProdukcji() {
        return rokProdukcji;
    }

    public abstract String dzwiek();

    public boolean equals(Object otherObject){
        if(this == otherObject){
            return true;
        }

        if(otherObject == null){
            return false;
        }
        if(getClass() != otherObject.getClass()){
            return false;
        }

        Instrument other = (Instrument) otherObject;
        return other.getProducent().equals(this.getProducent()) && other.getRokProdukcji().equals(this.getRokProdukcji());
    }

    @Override
    public String toString() {
        return "Instrument{" +
                "producent='" + producent + '\'' +
                ", rokProdukcji=" + rokProdukcji +
                '}';
    }
}
