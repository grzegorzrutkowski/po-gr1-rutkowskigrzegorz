package pl.edu.uwm.wmii.rutkowski.laboratorium08;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class zad1
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];
        String[] imiona = {"Tomek", "Grzesiek"};
        LocalDate now = LocalDate.now();

        ludzie[0] = new Pracownik("Kowalski ", imiona, now, false, 3000, now);
        ludzie[1] = new Student("Rutkowski ", imiona, now, false, "Informatyka", 4.3);

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko()  + p.getFormattedImiona() + " " + p.getFormattedPlec() + " urodzony: " + p.getFormattedData(p.getDataUrodzenia()) +  ": " + p.getOpis());
        }
    }
}

abstract class Osoba
{
    private String nazwisko;
    private String[] imiona;
    private LocalDate dataUrodzenia;
    private boolean plec;

    /**
     *
     * @param nazwisko
     * @param imiona
     * @param dataUrodzenia
     * @param plec (1 - kobieta, 0 - mężczyzna)
     */
    public Osoba(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec)
    {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return this.nazwisko;
    }

    public String[] getImiona(){
        return this.imiona;
    }

    public String getFormattedImiona(){
        StringBuilder temp = new StringBuilder("");
        for(String imie : this.imiona){
            temp.append(imie).append(" ");
        }
        return temp.toString();
    }

    public String getFormattedPlec(){
        if(this.plec){
            return "kobieta";
        } else {
            return "mężczyzna";
        }
    }

    public LocalDate getDataUrodzenia(){
        return this.dataUrodzenia;
    }

    public boolean getPlec(){
        return this.plec;
    }

    public String getFormattedData(LocalDate localDate){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");
        return localDate.format(formatter);
    }
}

class Pracownik extends Osoba
{
    private double pobory;
    private LocalDate dataZatrudnienia;

    public Pracownik(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, double pobory, LocalDate dataZatrudnienia)
    {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory()
    {
        return this.pobory;
    }

    public LocalDate getDataZatrudnienia(){
        return this.dataZatrudnienia;
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł, zatrudniony %s", pobory, this.getFormattedData(this.dataZatrudnienia));
    }
}


class Student extends Osoba
{
    private String kierunek;
    private double sredniaOcen;

    public Student(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, String kierunek, double sredniaOcen)
    {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis()
    {
        return "kierunek studiów: " + this.kierunek + " srednia ocen: " + this.sredniaOcen;
    }

    public double getSredniaOcen(){
        return this.sredniaOcen;
    }
}

