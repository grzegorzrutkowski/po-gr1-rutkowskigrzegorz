package pl.edu.uwm.wmii.rutkowski.laboratorium08;

public class Flet extends Instrument{

    public Flet(String producent, String rokProdukcji) {
        super(producent, rokProdukcji);
    }

    @Override
    public String dzwiek() {
        return "fiufiu";
    }
}
