package pl.edu.uwm.wmii.rutkowski.laboratorium01;

import java.util.Scanner;


public class zadanie1i {
    private static int silnia(int i)
    {
        if (i < 1)
            return 1;
        else
            return i * silnia(i - 1);
    }

    public static void main(String[] args) {

        float suma = 0;

        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);
        float n = scanner.nextFloat();

        for(int i = 1; i<=n; i++)
        {
            float liczba = scanner.nextFloat();
            suma += (Math.pow(-1, i) * liczba) / silnia(i);
        }
        System.out.println(suma);
    }
}
