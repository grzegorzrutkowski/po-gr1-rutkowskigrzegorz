package pl.edu.uwm.wmii.rutkowski.laboratorium01;

import java.util.Scanner;

public class zadanie23 {
    public static void main(String[] args) {
        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int suma = 0;

        for(int i = 1; i<=n; i++)
        {
            int liczba = scanner.nextInt();
            if(liczba > 0){
                suma += liczba;
            }

        }

        suma *= 2;

        System.out.print(suma);

    }
}
