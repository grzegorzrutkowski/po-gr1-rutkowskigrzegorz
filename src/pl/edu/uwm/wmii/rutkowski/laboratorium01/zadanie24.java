package pl.edu.uwm.wmii.rutkowski.laboratorium01;

import java.util.Scanner;

public class zadanie24 {
    public static void main(String[] args) {
        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int min = 0;
        int max = 0;
        boolean set_min = false;
        boolean set_max = false;

        for(int i = 1; i<=n; i++)
        {
            int liczba = scanner.nextInt();
            if(!set_min){
                set_min = true;
                min = liczba;
            }
            if(!set_max){
                set_max = true;
                max = liczba;
            }
            if(liczba < min){
                min = liczba;
            }
            else if(liczba > max){
                max = liczba;
            }
        }

        System.out.println(min);
        System.out.println(max);
    }
}
