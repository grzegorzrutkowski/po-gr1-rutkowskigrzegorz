package pl.edu.uwm.wmii.rutkowski.laboratorium01;

import java.util.Scanner;

public class zadanie22 {
    public static void main(String[] args) {
        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int dodatnie = 0;
        int ujemne = 0;
        int zera = 0;
        for(int i = 1; i<=n; i++)
        {
            int liczba = scanner.nextInt();
            if(liczba > 0){
                dodatnie++;
            }
            else if(liczba < 0 ){
                ujemne++;
            }
            else {
                zera++;
            }
        }

        System.out.println(dodatnie);
        System.out.println(ujemne);
        System.out.println(zera);

    }
}
