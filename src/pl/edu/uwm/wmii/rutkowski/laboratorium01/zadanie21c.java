package pl.edu.uwm.wmii.rutkowski.laboratorium01;

import java.util.Scanner;

public class zadanie21c {
    public static void main(String[] args) {
        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int ile = 0;

        for(int i = 0; i<n; i++)
        {
            int liczba = scanner.nextInt();
            if(liczba % 2 == 0 && Math.sqrt(liczba) == (int)Math.sqrt(liczba)){
                ile++;
            }

        }

        System.out.print(ile);

    }
}
