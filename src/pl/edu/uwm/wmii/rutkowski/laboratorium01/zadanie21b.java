package pl.edu.uwm.wmii.rutkowski.laboratorium01;

import java.util.Scanner;

public class zadanie21b {
    public static void main(String[] args) {
        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int ile = 0;

        for(int i = 0; i<n; i++)
        {
            int liczba = scanner.nextInt();
            if(liczba % 3 == 0 && liczba % 5 != 0){
                ile++;
            }

        }

        System.out.print(ile);

    }
}
