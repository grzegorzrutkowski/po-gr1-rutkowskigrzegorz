package pl.edu.uwm.wmii.rutkowski.laboratorium01;

import java.util.Scanner;

public class zadanie21e {

    private static int silnia(int i)
    {
        if (i < 1)
            return 1;
        else
            return i * silnia(i - 1);
    }

    public static void main(String[] args) {
        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int ile = 0;

        for(int i = 0; i<n; i++)
        {
            int liczba = scanner.nextInt();
            if(Math.pow(2, liczba) < liczba && liczba < silnia(liczba)){
                ile++;
            }

        }

        System.out.print(ile);

    }
}
