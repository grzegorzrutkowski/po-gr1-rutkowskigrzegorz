package pl.edu.uwm.wmii.rutkowski.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad1g {
    public static void main(String[] args) {
        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        if(n < 1 || n > 100){
            System.err.println("Liczba musi być z zakresu 1 <= n <= 100");
            System.exit(1);
        }
        int[] tab = new int[n];
        Random r = new Random();
        for (int j = 0; j < tab.length; ++j) {
            tab[j] = r.nextInt(1999) - 999;
        }

        for (int el : tab) {
            System.out.print(el + " ");
        }

        System.out.println("Podaj lewą wartosc: ");
        int lewa = scanner.nextInt();

        if(1 > lewa || lewa > n){
            System.err.println("Liczba musi być z zakresu < n & > n");
            System.exit(1);
        }

        System.out.println("Podaj prawą wartosc: ");
        int prawa = scanner.nextInt();

        if(1 > prawa || prawa > n){
            System.err.println("Liczba musi być z zakresu < n & > n");
            System.exit(1);
        }


        System.out.println("Po zmianie: ");

        for (int el : tab) {
            System.out.print(el + " ");
        }
    }
}
