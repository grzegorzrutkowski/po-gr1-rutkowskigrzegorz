package pl.edu.uwm.wmii.rutkowski.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad2a {

    public static int ileNieparzystych(int[] tab){
        int ile = 0;
        for (int el : tab) {
            if(el % 2 != 0){
                ile++;
            }
        }
        return ile;
    }

    public static int ileParzystych(int[] tab){
        int ile = 0;
        for (int el : tab) {
            if(el % 2 == 0){
                ile++;
            }
        }
        return ile;
    }

    public static void main(String[] args) {
        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        if(n < 1 || n > 100){
            System.err.println("Liczba musi być z zakresu 1 <= n <= 100");
            System.exit(1);
        }
        int[] tab = new int[n];
        generuj(tab, -999, 999);

        System.out.println("");
        System.out.println("Nieparzystych: " + ileNieparzystych(tab) + " Parzystych:" + ileParzystych(tab));
    }

    // generuje pseudolosowe liczby całkowite z przedziału [0..max)
    public static void generuj (int[] tab, int min, int max) {
        Random r = new Random();
        for (int j = 0; j < tab.length; ++j) {
            tab[j] = r.nextInt(max + 1 - min) + min;
        }
    }
}
