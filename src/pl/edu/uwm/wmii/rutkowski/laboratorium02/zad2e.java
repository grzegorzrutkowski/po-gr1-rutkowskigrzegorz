package pl.edu.uwm.wmii.rutkowski.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad2e {

    public static int dlugoscMaksymalnegoCiaguDodatnich(int[] tab){
        int dlugosc = 0;
        int najdluzszy = 0;

        for (int el : tab) {
            System.out.print(el + " ");
            if(el > 0){
                dlugosc++;
            } else {
                if(dlugosc != 0 && dlugosc > najdluzszy){
                    najdluzszy = dlugosc;
                }
                dlugosc = 0;
            }
        }
        return najdluzszy;
    }

    public static void main(String[] args) {
        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        if(n < 1 || n > 100){
            System.err.println("Liczba musi być z zakresu 1 <= n <= 100");
            System.exit(1);
        }
        int[] tab = new int[n];
        generuj(tab, -999, 999);

        System.out.println("");
        System.out.println("Maks Dlugosc ciagu liczb dodatnich: " + dlugoscMaksymalnegoCiaguDodatnich(tab));
    }

    // generuje pseudolosowe liczby całkowite z przedziału [0..max)
    public static void generuj (int[] tab, int min, int max) {
        Random r = new Random();
        for (int j = 0; j < tab.length; ++j) {
            tab[j] = r.nextInt(max + 1 - min) + min;
        }
    }
}
