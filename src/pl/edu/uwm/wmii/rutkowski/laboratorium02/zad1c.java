package pl.edu.uwm.wmii.rutkowski.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad1c {
    public static void main(String[] args) {
        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        if(n < 1 || n > 100){
            System.err.println("Liczba musi być z zakresu 1 <= n <= 100");
            System.exit(1);
        }
        int[] tab = new int[n];
        Random r = new Random();
        for (int j = 0; j < tab.length; ++j) {
            tab[j] = r.nextInt(1999) - 999;
        }

        int najwiekszy = tab[0];
        int ile = 1;

        for(int i = 1; i<tab.length; i++){
            System.out.print(tab[i] + " ");
            if(tab[i] == najwiekszy){
                ile++;
            } else if(tab[i] > najwiekszy){
                ile = 1;
                najwiekszy = tab[i];
            }
        }

        System.out.println("");
        System.out.println("Najwiekszy element: " + najwiekszy + " wystepuje: " + ile + " razy");
    }
}
