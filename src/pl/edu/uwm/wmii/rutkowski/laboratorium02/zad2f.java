package pl.edu.uwm.wmii.rutkowski.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad2f {

    public static void signum(int[] tab){
        for(int i = 0; i<tab.length; i++){
            if(tab[i] < 0){
                tab[i] = -1;
            } else if(tab[i] > 0){
                tab[i] = 1;
            }
        }
    }

    public static void main(String[] args) {
        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        if(n < 1 || n > 100){
            System.err.println("Liczba musi być z zakresu 1 <= n <= 100");
            System.exit(1);
        }
        int[] tab = new int[n];
        generuj(tab, -999, 999);
        for (int el : tab) {
            System.out.print(el + " ");
        }

        signum(tab);
        System.out.println("");

        for (int el : tab) {
            System.out.print(el + " ");
        }
    }

    // generuje pseudolosowe liczby całkowite z przedziału [0..max)
    public static void generuj (int[] tab, int min, int max) {
        Random r = new Random();
        for (int j = 0; j < tab.length; ++j) {
            tab[j] = r.nextInt(max + 1 - min) + min;
        }
    }
}
