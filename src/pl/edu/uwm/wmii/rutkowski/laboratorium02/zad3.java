package pl.edu.uwm.wmii.rutkowski.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad3 {
    public static int[][] iloczyn(int[][] a, int[][] b, int m, int p, int o) {
        int[][] c = new int[m][p];
        for( int i = 1; i <= m; i++ )
        {
            for( int j = 1; j <= p; j++ )
            {
                c[i][j] = 0;
                for( int k = 1; k <= o; k++ )
                {
                    c[i][j] = c[i][j] + a[i][k] * b[k][j];
                }
            }
        }
        return c;
    }

    public static void main(String[] args) {
        System.out.println("Podaj liczbe m: ");
        Scanner scanner = new Scanner(System.in);

        int m = scanner.nextInt();
        if(m < 1 || m > 10){
            System.err.println("Liczba musi być z zakresu [1,10]");
            System.exit(1);
        }

        System.out.println("Podaj liczbe n: ");
        int n = scanner.nextInt();
        if(n < 1 || n > 100){
            System.err.println("Liczba musi być z zakresu [1,10]");
            System.exit(1);
        }

        System.out.println("Podaj liczbe k: ");
        int k = scanner.nextInt();
        if(k < 1 || k > 100){
            System.err.println("Liczba musi być z zakresu [1,10]");
            System.exit(1);
        }
        int[][] a = new int[m][n];
        int[][] b = new int[n][k];

        generuj(a, m, n, -999, 999);
        generuj(b, n, k, -999, 999);

        wypisz(a, m, n);
        System.out.println("");
        wypisz(b, m, n);

        System.out.println("Iloczyn: ");

        int[][] c = iloczyn(a, b, m, k, n);

        wypisz(c, m, k);
    }

    public static void generuj (int[][] tab, int n, int m, int min, int max) {
        Random r = new Random();
        for (int j = 0; j < n; j++) {
            for (int k = 0; k < m; k++) {
                tab[j][k] = r.nextInt(max + 1 - min) + min;
            }
        }
    }

    public static void wypisz (int[][] tab, int n, int m) {
        for (int j = 0; j < n; ++j) {
            for (int k = 0; k < m; ++k) {
                System.out.print(tab[j][k] + " ");
            }
            System.out.println("");
        }
    }
}
