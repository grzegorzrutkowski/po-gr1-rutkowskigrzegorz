package pl.edu.uwm.wmii.rutkowski.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad1b {
    public static void main(String[] args) {
        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        if(n < 1 || n > 100){
            System.err.println("Liczba musi być z zakresu 1 <= n <= 100");
            System.exit(1);
        }
        int[] tab = new int[n];
        Random r = new Random();
        for (int j = 0; j < tab.length; ++j) {
            tab[j] = r.nextInt(1999) - 999;
        }

        int ujemn = 0;
        int dodat = 0;
        int zera = 0;

        for (int el : tab) {
            System.out.print(el + " ");
            if(el < 0){
                ujemn++;
            } else if(el > 0){
                dodat++;
            } else {
                zera++;
            }
        }
        System.out.println("");
        System.out.println("Dodatnich: " + dodat + " Ujemnych:" + ujemn +  " Zer: " + zera);
    }
}
