package pl.edu.uwm.wmii.rutkowski.laboratorium02;

import java.util.Random;
import java.util.Scanner;

public class zad2g {

    public static void odwrocFragment(int[] tab, int lewa, int prawa){
        for(int i = lewa; i<prawa; i++){
            int temp = tab[i];
            tab[i] = tab[prawa - i - 1];
            tab[prawa - i - 1] = temp;
        }
    }

    public static void main(String[] args) {
        System.out.println("Podaj liczbe n: ");
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        if(n < 1 || n > 100){
            System.err.println("Liczba musi być z zakresu 1 <= n <= 100");
            System.exit(1);
        }
        int[] tab = new int[n];
        generuj(tab, -999, 999);

        System.out.println("Podaj lewą wartosc: ");
        int lewa = scanner.nextInt();

        if(1 > lewa || lewa > n){
            System.err.println("Liczba musi być z zakresu < n & > n");
            System.exit(1);
        }

        System.out.println("Podaj prawą wartosc: ");
        int prawa = scanner.nextInt();

        if(1 > prawa || prawa > n){
            System.err.println("Liczba musi być z zakresu < n & > n");
            System.exit(1);
        }

        for (int el : tab) {
            System.out.print(el + " ");
        }

        odwrocFragment(tab, lewa, prawa);
        System.out.println("");

        for (int el : tab) {
            System.out.print(el + " ");
        }
    }

    // generuje pseudolosowe liczby całkowite z przedziału [0..max)
    public static void generuj (int[] tab, int min, int max) {
        Random r = new Random();
        for (int j = 0; j < tab.length; ++j) {
            tab[j] = r.nextInt(max + 1 - min) + min;
        }
    }
}
