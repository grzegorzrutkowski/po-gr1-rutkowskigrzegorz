package pl.edu.uwm.wmii.rutkowski.laboratorium04;

public class zad1a {
    public static int countChar(String str, char c){
        int ile = 0;
        for(int i = 0; i<str.length(); i++){
            if(str.charAt(i) == c){
                ile++;
            }
        }
        return ile;
    }
    public static void main(String[] args) {
        System.out.print("a w ala: " + countChar("ala", 'a'));
    }
}
