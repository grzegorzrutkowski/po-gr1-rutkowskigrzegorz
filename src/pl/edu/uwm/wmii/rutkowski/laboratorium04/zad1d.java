package pl.edu.uwm.wmii.rutkowski.laboratorium04;

import java.util.Scanner;

public class zad1d {
    public static String repeat(String str, int n){
        StringBuilder temp = new StringBuilder("");
        for(int i = 0; i<n; i++){
            temp.append(str);
        }
        return temp.toString();
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Podaj napis ");
        String napis = in.nextLine();

        System.out.println(napis + " x3: " + repeat(napis, 3));
    }
}
