package pl.edu.uwm.wmii.rutkowski.laboratorium04;

public class zad1b {
    public static int countSubStr(String str, String subStr){
        int atIndex = 0;
        int count = 0;

        while (atIndex != -1)
        {
            atIndex = str.indexOf(subStr, atIndex);

            if(atIndex != -1)
            {
                count++;
                atIndex += subStr.length();
            }
        }
        return count;
    }
    public static void main(String[] args) {
        System.out.print("ala w: ala ma kota ala alaala: " + countSubStr("ala ma kota ala alaala", "ala"));
    }
}
