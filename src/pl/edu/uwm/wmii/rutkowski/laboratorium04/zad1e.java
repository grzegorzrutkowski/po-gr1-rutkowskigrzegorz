package pl.edu.uwm.wmii.rutkowski.laboratorium04;

import java.util.Arrays;
import java.util.Scanner;

public class zad1e {
    public static int[] where(String str, String subStr){
        int atIndex = 0;
        int count = 0;
        int[] tab = new int[str.length()];

        while (atIndex != -1)
        {
            atIndex = str.indexOf(subStr, atIndex);

            if(atIndex != -1)
            {
                tab[atIndex] = atIndex;
                count++;
                atIndex += subStr.length();
            }
        }
        return tab;
    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Podaj napis ");
        String napis = in.nextLine();
        System.out.println("Podaj podnapis ");
        String podnapis = in.nextLine();

        //look for all != 0
        System.out.println("Ala ma kota" + ": " + podnapis  + " : " + Arrays.toString(where(napis, podnapis)));
    }
}
