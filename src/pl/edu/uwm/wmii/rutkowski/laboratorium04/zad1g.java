package pl.edu.uwm.wmii.rutkowski.laboratorium04;

import java.util.Arrays;
import java.util.Scanner;

public class zad1g {
    public static String nice(String str){
        StringBuffer msg = new StringBuffer("");
        int licznik = 0;
        for (int i = str.length()-1; i >= 0; i--){
            if(licznik == 3){
                msg.append('\'');
                licznik = 0;
            }
            msg.append(str.charAt(i));
            licznik++;
        }
        msg = msg.reverse();
        return msg.toString();

    }
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Podaj napis ");
        String napis = in.nextLine();

        System.out.println(napis + ": " + nice(napis));
    }
}
