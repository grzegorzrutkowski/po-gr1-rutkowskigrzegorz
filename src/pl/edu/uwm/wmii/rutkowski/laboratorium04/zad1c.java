package pl.edu.uwm.wmii.rutkowski.laboratorium04;

public class zad1c {
    public static String middle(String str){
        int length = str.length();
        if(length % 2 == 0){
            return str.charAt(str.length() / 2 - 1) + Character.toString(str.charAt(str.length()/2));
        } else {
            return Character.toString(str.charAt(str.length()/2));
        }
    }
    public static void main(String[] args) {
        System.out.println("middle: " + middle("middle"));
        System.out.println("tomek: " + middle("tomek"));
    }
}
