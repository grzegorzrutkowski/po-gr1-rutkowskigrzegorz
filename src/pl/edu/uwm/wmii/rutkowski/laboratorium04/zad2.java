package pl.edu.uwm.wmii.rutkowski.laboratorium04;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class zad2 {
    public static int countChar(String str, char c){
        int ile = 0;
        for(int i = 0; i<str.length(); i++){
            if(str.charAt(i) == c){
                ile++;
            }
        }
        return ile;
    }

    public static void main(String[] args) throws FileNotFoundException
    {
        if (args.length != 2) {
            System.err.println("Sposób użycia: java zad2 nazwaPliku znak");
            System.exit(1);
        }

        Scanner file = new Scanner(new File(args[0]));

        int ile = 0;

        while (file.hasNextLine()) {
            String line = file.nextLine();
            ile += countChar(line, args[1].charAt(0));
        }

        System.out.println("\nIlość wystapien " + args[1] + "w pliku: " + args[0] + " : " + ile);
    }
}
