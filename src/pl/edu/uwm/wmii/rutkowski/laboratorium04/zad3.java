package pl.edu.uwm.wmii.rutkowski.laboratorium04;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class zad3 {
    public static int countSubStr(String str, String subStr){
        int atIndex = 0;
        int count = 0;

        while (atIndex != -1)
        {
            atIndex = str.indexOf(subStr, atIndex);

            if(atIndex != -1)
            {
                count++;
                atIndex += subStr.length();
            }
        }
        return count;
    }

    public static void main(String[] args) throws FileNotFoundException
    {
        if (args.length != 2) {
            System.err.println("Sposób użycia: java zad3 nazwaPliku wyraz");
            System.exit(1);
        }

        Scanner file = new Scanner(new File(args[0]));

        int ile = 0;

        while (file.hasNextLine()) {
            String line = file.nextLine();
            ile += countSubStr(line, args[1]);
        }

        System.out.println("\nIlość wystapien " + args[1] + "w pliku: " + args[0] + " : " + ile);
    }
}
