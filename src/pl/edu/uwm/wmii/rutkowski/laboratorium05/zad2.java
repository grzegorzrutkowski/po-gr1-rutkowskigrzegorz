package pl.edu.uwm.wmii.rutkowski.laboratorium05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Scanner;

public class zad2 {
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> c = new ArrayList<>();
        if(a.size() < b.size()){
            for (int counter = 0; counter < b.size(); counter++) {
                c.add(b.get(counter));
                if(counter < a.size()){
                    c.add(a.get(counter));
                }
            }
        } else {
            for (int counter = 0; counter < a.size(); counter++) {
                c.add(a.get(counter));
                if(counter < b.size()){
                    c.add(b.get(counter));
                }
            }
        }
        return c;
    }

    public static void main(String[] args) {
        int[] ids = {-3, 0, 100, 8, 300, 500};
        int[] ids2 = {5, 8, 200};

        ArrayList<Integer> a = new ArrayList<>();
        ArrayList<Integer> b = new ArrayList<>();

        for (int id: ids) {
            a.add(id);
        }
        for(int id: ids2) {
            b.add(id);
        }

        System.out.println(a);
        System.out.println(b);
        ArrayList<Integer> c = merge(a, b);
        System.out.println(c);
    }

}
