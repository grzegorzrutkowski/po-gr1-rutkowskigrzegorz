package pl.edu.uwm.wmii.rutkowski.laboratorium05;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class zad5 {

    public static void reverse(ArrayList<Integer> a){

        for (int counter = 0; counter < a.size() / 2; counter++) {
            int temp = a.get(counter);
            a.set(counter, a.get(a.size() - counter - 1));
            a.set(a.size() - counter - 1, temp);
        }
    }

    public static void main(String[] args) {
        int[] ids = {-3, 0, 100, 8, 300, 500};

        ArrayList<Integer> a = new ArrayList<>();

        for (int id: ids) {
            a.add(id);
        }

        System.out.println(a);
        reverse(a);
        System.out.println(a);
    }

}
