package pl.edu.uwm.wmii.rutkowski.laboratorium00;

public class zadanie4 {
    public static void main(String[] args)
    {
        double suma = 1000.00;
        System.out.println("Start: " + suma);
        suma += 0.06 * suma;
        System.out.println("Po pierwszym roku: " + suma);
        suma += 0.06 * suma;
        System.out.println("Po drugim roku: " + suma);
        suma += 0.06 * suma;
        System.out.println("Po trzecim roku: " + suma);
    }
}
