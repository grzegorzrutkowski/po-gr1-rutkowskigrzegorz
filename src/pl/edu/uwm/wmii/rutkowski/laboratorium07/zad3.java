package pl.edu.uwm.wmii.rutkowski.laboratorium07;

import pl.imiajd.rutkowski.Nauczyciel;
import pl.imiajd.rutkowski.Osoba;
import pl.imiajd.rutkowski.Student;

public class zad3 {
    public static void main(String[] args){
        Osoba o1 = new Osoba("Rutkowski" , 2000);
        System.out.println(o1.toString());

        System.out.println("  ");

        Student s1 = new Student("Rutkowski", 2000, "Informatyka");
        System.out.println(s1.toString());

        System.out.println("  ");

        Nauczyciel n1 = new Nauczyciel("Rutkowski", 2000, 3800);
        System.out.println(n1.toString());
    }
}
