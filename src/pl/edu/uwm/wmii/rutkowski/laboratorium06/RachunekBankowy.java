package pl.edu.uwm.wmii.rutkowski.laboratorium06;

public class RachunekBankowy {
    static double rocznaStopaProcentowa;
    private double saldo;

    public RachunekBankowy(double saldo){
        this.saldo = saldo;
    }

    public void obliczMiesieczneOdsetki(){
        this.saldo += (this.saldo * rocznaStopaProcentowa) / 12;
    }

    public static void setRocznaStopaProcentowa(double stopa){
        rocznaStopaProcentowa = stopa;
    }

    public double getSaldo(){
        return this.saldo;
    }
}
