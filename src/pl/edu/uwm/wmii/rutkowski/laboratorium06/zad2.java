package pl.edu.uwm.wmii.rutkowski.laboratorium06;

public class zad2 {
    public static void main(String[] args) throws Exception {
        IntegerSet i1 = new IntegerSet();
        System.out.println(i1.toString());

        i1.insertElement(5);
        i1.insertElement(10);


        i1.deleteElement(5);
        System.out.println(i1.toString());

        IntegerSet i2 = new IntegerSet();

        i2.insertElement(9);
        i2.insertElement(10);
        i2.insertElement(12);
        System.out.println(i2.toString());

        IntegerSet i3 = IntegerSet.union(i1, i2);
        System.out.println(i3.toString());

        IntegerSet i4 = IntegerSet.intersection(i1, i2);
        System.out.println(i4.toString());

        System.out.println(i4.equals(i2));

    }

}
