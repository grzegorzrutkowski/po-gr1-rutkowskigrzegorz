package pl.edu.uwm.wmii.rutkowski.laboratorium06;

import java.util.Arrays;

public class IntegerSet {
    private final Boolean[] zbior;

    public IntegerSet(){
        this.zbior = new Boolean[100];
        for(int i = 0; i<100; i++){
           this.zbior[i] = false;
        }
    }

    public void insertElement(int liczba) throws Exception {
        if(liczba < 1 || liczba > 100){
            throw new Exception("Zła liczba");
        }
        this.zbior[liczba-1] = true;
    }

    public Boolean getElement(int i){
        return this.zbior[i];
    }

    public static IntegerSet union(IntegerSet i1, IntegerSet i2) throws Exception {
        IntegerSet i3 = new IntegerSet();
        for(int i = 0; i<100; i++){
            if(i1.getElement(i) || i2.getElement(i)){
               i3.insertElement(i+1);
            }
        }
        return i3;
    }

    public static IntegerSet intersection(IntegerSet i1, IntegerSet i2) throws Exception {
        IntegerSet i3 = new IntegerSet();
        for(int i = 0; i<100; i++){
            if(i1.getElement(i) && i2.getElement(i)){
                i3.insertElement(i+1);
            }
        }
        return i3;
    }

    public void deleteElement(int liczba) throws Exception {
        if(liczba < 1 || liczba > 100){
            throw new Exception("Zła liczba");
        }
        this.zbior[liczba-1] = false;
    }

    @Override
    public String toString() {
        StringBuilder napis = new StringBuilder("");
        for(int i = 0; i<100; i++){
            if(this.zbior[i]){
                napis.append(i + 1).append(",");
            }
        }
        return napis.toString();
    }

    public boolean equals(Object otherObject){
        if(this == otherObject){
            return true;
        }

        if(otherObject == null){
            return false;
        }
        if(getClass() != otherObject.getClass()){
            return false;
        }

        IntegerSet other = (IntegerSet) otherObject;

        for(int i = 0; i<100; i++){
            if(this.zbior[i] != other.getElement(i)){
                return false;
            }
        }
        return true;
    }
}
