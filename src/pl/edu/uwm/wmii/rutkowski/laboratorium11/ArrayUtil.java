package pl.edu.uwm.wmii.rutkowski.laboratorium11;

import java.time.LocalDate;
import java.util.ArrayList;

class ArrayUtil {
    public static void main (String[] args) {
        int[] k1 = { 3, 5, 7, 8, 10 };
        int[] k2 = { 10, 9, 8, 7, 6 };
        int[] k3 = { 6, 4, 20, 40 };
        int[] k4 = { 2, -190, 30, 8 };

        ArrayList<Integer> l1 = new ArrayList<>();
        ArrayList<Integer> l2 = new ArrayList<>();
        ArrayList<Integer> l3 = new ArrayList<>();
        ArrayList<Integer> l4 = new ArrayList<>();

        ArrayList<LocalDate> data1 = new ArrayList<>();
        ArrayList<LocalDate> data2 = new ArrayList<>();
        ArrayList<LocalDate> data3 = new ArrayList<>();

        data1.add(LocalDate.of(2000, 5, 30));
        data1.add(LocalDate.of(2000, 6, 30));
        data1.add(LocalDate.of(2000, 7, 30));

        data2.add(LocalDate.of(2001, 5, 30));
        data2.add(LocalDate.of(2000, 6, 30));
        data2.add(LocalDate.of(1999, 7, 30));

        data3.add(LocalDate.of(2001, 5, 30));
        data3.add(LocalDate.of(2000, 6, 30));
        data3.add(LocalDate.of(1999, 7, 30));


        for (int id: k1) {
            l1.add(id);
        }

        for (int id: k2) {
            l2.add(id);
        }

        for (int id: k3) {
            l3.add(id);
        }

        for (int id: k4) {
            l4.add(id);
        }

        System.out.println("l1: " + ArrayUtil.isSorted(l1));
        System.out.println("l2: " + ArrayUtil.isSorted(l2));
        System.out.println("l3: " + ArrayUtil.isSorted(l3));

        System.out.println("data1: " + ArrayUtil.isSorted(data1));
        System.out.println("data2: " + ArrayUtil.isSorted(data2));
        System.out.println("data3: " + ArrayUtil.isSorted(data3));

        System.out.println("binSearch l1 5: " + ArrayUtil.binSearch(l1, 5));
        System.out.println("binSearch l1 3: " + ArrayUtil.binSearch(l1, 3));
        ArrayUtil.selectionSort(l3);
        System.out.println("Po posortowaniu l3:");

        System.out.println("l3: " + l3);

        //ArrayUtil.mergeSort(l4);
        System.out.println("Po posortowaniu l4:");

        System.out.println("l4: " + l4);
    }

    private static <T extends Comparable<? super T>> boolean isSorted(ArrayList<T> array){
        for (int i = 0; i < array.size()-1; i++) {
            if(array.get(i).compareTo(array.get(i+1))> 0){
                return false;
            }
        }
        return true;
    }

    private static <T extends Comparable<? super T>> void selectionSort(ArrayList<T> arr) {
        for (int i = 0; i < arr.size() - 1; i++) {
            int minElementIndex = i;
            for (int j = i + 1; j < arr.size(); j++) {
                if (arr.get(minElementIndex).compareTo(arr.get(j)) > 0) {
                    minElementIndex = j;
                }
            }

            if (minElementIndex != i) {
                T temp = arr.get(i);
                arr.set(i, arr.get(minElementIndex));
                arr.set(minElementIndex, temp);
            }
        }
    }

    private static <T extends Comparable<? super T>> ArrayList<T> mergeSort(ArrayList<T> arr) {
        ArrayList<T> left = new ArrayList<T>();
        ArrayList<T> right = new ArrayList<T>();

        int middle = arr.size() / 2; //int division
        for(int i=0;i<middle;i++) {
            left.add(arr.get(i));
        }
        for(int i=middle;i<arr.size();i++) {
            right.add(arr.get(i));
        }

        return merge(mergeSort(left), mergeSort(right));
    }

    private static <T extends Comparable<? super T>> ArrayList<T> merge(ArrayList<T> a, ArrayList<T> b) {
        ArrayList<T> ret = new ArrayList<T>();	//return list
        int a_idx = 0, b_idx = 0;			//counters of items left in respective lists

        while(a_idx+1 <= a.size() || b_idx+1 <= b.size()) {
            if(a_idx+1 <= a.size() && b_idx+1 <= b.size()) {
                if(a.get(a_idx).compareTo(b.get(b_idx)) <= 0.0) {
                    ret.add(a.get(a_idx));
                    a_idx++;
                } else {
                    ret.add(b.get(b_idx));
                    b_idx++;
                }
            } else if(a_idx+1 <= a.size()) {
                ret.add(a.get(a_idx));
                a_idx++;
            } else if(b_idx+1 <= b.size()) {
                ret.add(b.get(b_idx));
                b_idx++;
            }
        }
        return ret;
    }


    private static <T extends Comparable<? super T>> int binSearch(ArrayList<T> array, T obj) {
        for(int i = 0; i<array.size(); i++){
            if(array.get(i).equals(obj)){
                return i;
            }
        }
        return -1;
    }
}
