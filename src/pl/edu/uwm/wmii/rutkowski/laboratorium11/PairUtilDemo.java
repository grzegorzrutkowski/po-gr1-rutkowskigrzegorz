package pl.edu.uwm.wmii.rutkowski.laboratorium11;

public class PairUtilDemo<T> extends Pair<T> {
    public static void main (String[] args) {
        String[] words = { "Ala", "ma", "psa", "i", "kota" };
        Pair<String> mm = ArrayAlg.minmax(words);
        System.out.println("min = " + mm.getFirst());
        System.out.println("max = " + mm.getSecond());
        Pair<String> newObj = PairUtil.swap(mm);
        System.out.println("After swap:");

        System.out.println("min = " + newObj.getFirst());
        System.out.println("max = " + newObj.getSecond());
    }
}

