package pl.edu.uwm.wmii.rutkowski.laboratorium11;

public class PairUtil{
    public static <T> Pair<T> swap(Pair<T> obj) {
        return new Pair<T>(obj.getSecond(), obj.getFirst());
    }
}