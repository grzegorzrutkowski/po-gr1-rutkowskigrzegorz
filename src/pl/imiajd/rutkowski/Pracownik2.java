package pl.imiajd.rutkowski;

import java.time.LocalDate;

public class Pracownik2 extends Osoba2
{
    private double pobory;
    private LocalDate dataZatrudnienia;

    public Pracownik2(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, double pobory, LocalDate dataZatrudnienia)
    {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory()
    {
        return this.pobory;
    }

    public LocalDate getDataZatrudnienia(){
        return this.dataZatrudnienia;
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł, zatrudniony %s", pobory, this.getFormattedData(this.dataZatrudnienia));
    }
}
