package pl.imiajd.rutkowski;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public abstract class Osoba2
{
    private String nazwisko;
    private String[] imiona;
    private LocalDate dataUrodzenia;
    private boolean plec;

    /**
     *
     * @param nazwisko
     * @param imiona
     * @param dataUrodzenia
     * @param plec (1 - kobieta, 0 - mężczyzna)
     */
    public Osoba2(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec)
    {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return this.nazwisko;
    }

    public String[] getImiona(){
        return this.imiona;
    }

    public String getFormattedImiona(){
        StringBuilder temp = new StringBuilder("");
        for(String imie : this.imiona){
            temp.append(imie).append(" ");
        }
        return temp.toString();
    }

    public String getFormattedPlec(){
        if(this.plec){
            return "kobieta";
        } else {
            return "mężczyzna";
        }
    }

    public LocalDate getDataUrodzenia(){
        return this.dataUrodzenia;
    }

    public boolean getPlec(){
        return this.plec;
    }

    public String getFormattedData(LocalDate localDate){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");
        return localDate.format(formatter);
    }
}