package pl.imiajd.rutkowski;

public class Nauczyciel extends Osoba {
    private double pensja;

    public Nauczyciel(String nazwisko, int rokUrodzenia, double pensja) {
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }

    public double getPensja(){
        return this.pensja;
    }

    public void setPensja(double pensja){
        this.pensja = pensja;
    }

    public String toString(){
        return "Nazwisko: " + this.nazwisko + "\n" + "Rok urodzenia: " + this.rokUrodzenia + "\nPensja:" + this.pensja;
    }
}
