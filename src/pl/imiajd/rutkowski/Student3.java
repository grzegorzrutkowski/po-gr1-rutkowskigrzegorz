package pl.imiajd.rutkowski;

public class Student3  extends Osoba3 implements Cloneable, Comparable {
    private double sredniaOcen;

    public Student3(String nazwisko, int year, int month, int day, double sredniaOcen) {
        super(nazwisko, year, month, day);
        this.sredniaOcen = sredniaOcen;
    }

    @Override
    public int compareTo(Object otherObject) {
        int parentCompare = super.compareTo(otherObject);
        if (parentCompare == 0) {
            Student3 other = (Student3) otherObject;
            return Double.compare(this.sredniaOcen, other.sredniaOcen);
        } else {
            return parentCompare;
        }
    }

    @Override
    public String toString() {
        return "Student3{" +
                "sredniaOcen=" + sredniaOcen +
                "} " + super.toString();
    }
}
