package pl.imiajd.rutkowski;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Osoba3 implements Comparable, Cloneable {
    private String nazwisko;
    private LocalDate dataUrodzenia;
    public Osoba3(String nazwisko, int year, int month, int day){
        this.nazwisko = nazwisko;
        this.dataUrodzenia = LocalDate.of(year, month, day);
    }

    public String getNazwisko(){
        return this.nazwisko;
    }

    public LocalDate getDataUrodzenia(){
        return this.dataUrodzenia;
    }

    @Override
    public int compareTo(Object otherObject) {
        Osoba3 other = (Osoba3) otherObject;
        int i = nazwisko.compareTo(other.nazwisko);
        if (i != 0) return i;

        return dataUrodzenia.compareTo(other.dataUrodzenia);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Osoba3 osoba3 = (Osoba3) o;
        return Objects.equals(nazwisko, osoba3.nazwisko) &&
                Objects.equals(dataUrodzenia, osoba3.dataUrodzenia);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nazwisko, dataUrodzenia);
    }

    @Override
    public String toString() {
        return "Osoba3{" +
                "nazwisko='" + nazwisko + '\'' +
                ", dataUrodzenia=" + this.dataUrodzenia.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) +
                '}';
    }
}
