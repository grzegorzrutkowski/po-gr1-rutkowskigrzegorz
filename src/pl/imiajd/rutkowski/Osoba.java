package pl.imiajd.rutkowski;

public class Osoba {
    protected String nazwisko;
    protected int rokUrodzenia;

    public Osoba(String nazwisko, int rokUrodzenia){
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
    }

    public String getNazwisko(){
        return this.nazwisko;
    }

    public int getRokUrodzenia(){
        return this.rokUrodzenia;
    }

    public void setNazwisko(String nazwisko){
        this.nazwisko = nazwisko;
    }

    public void setRokUrodzenia(int rokUrodzenia){
        this.rokUrodzenia = rokUrodzenia;
    }

    public String toString(){
        return "Nazwisko: " + this.nazwisko + "\n" + "Rok urodzenia: " + this.rokUrodzenia;
    }
}
