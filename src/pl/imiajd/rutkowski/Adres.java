package pl.imiajd.rutkowski;

public class Adres {
    private String ulica;
    private int numerDomu;
    private int numerMieszkania = 0;
    private String miasto;
    private String kodPocztowy;

    public Adres(String ulica, int numerDomu, int numerMieszkania, String miasto, String kodPocztowy){
        this.ulica = ulica;
        this.numerDomu = numerDomu;
        this.numerMieszkania = numerMieszkania;
        this.miasto = miasto;
        this.kodPocztowy = kodPocztowy;
    }
    public Adres(String ulica, int numerDomu, String miasto, String kodPocztowy){
        this.ulica = ulica;
        this.numerDomu = numerDomu;
        this.miasto = miasto;
        this.kodPocztowy = kodPocztowy;
    }

    public void Pokaz(){
        System.out.println(this.kodPocztowy + " " + this.miasto);
        if(this.numerMieszkania != 0){
            System.out.println(this.ulica + " " + this.numerDomu + "/" + this.numerMieszkania);
        } else {
            System.out.println(this.ulica + " " + this.numerDomu);
        }
    }
}
