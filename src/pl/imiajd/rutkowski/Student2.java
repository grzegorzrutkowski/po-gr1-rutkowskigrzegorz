package pl.imiajd.rutkowski;

import java.time.LocalDate;

public class Student2 extends Osoba2
{
    private String kierunek;
    private double sredniaOcen;

    public Student2(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, String kierunek, double sredniaOcen)
    {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis()
    {
        return "kierunek studiów: " + this.kierunek + " srednia ocen: " + this.sredniaOcen;
    }

    public double getSredniaOcen(){
        return this.sredniaOcen;
    }
}