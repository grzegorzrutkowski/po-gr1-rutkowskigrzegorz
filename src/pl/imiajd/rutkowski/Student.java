package pl.imiajd.rutkowski;

public class Student extends Osoba {
    private String kierunek;

    public Student(String nazwisko, int rokUrodzenia, String kierunek) {
        super(nazwisko, rokUrodzenia);
        this.kierunek = kierunek;
    }

    public String getKierunek(){
        return this.kierunek;
    }

    public void setKierunek(String kierunek){
        this.kierunek = kierunek;
    }

    public String toString(){
        return "Nazwisko: " + this.nazwisko + "\n" + "Rok urodzenia: " + this.rokUrodzenia + "\nKierunek:" + this.kierunek;
    }
}
